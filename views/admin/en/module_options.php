<?php

$aLang = array(
    'charset'                                          => 'UTF-8',
    'SHOP_MODULE_GROUP_mainExclude'                    => 'Exclude categories & articles',

    'SHOP_MODULE_odExcludeCategories'                  => 'List of the categories, where module will not be shown',
    'HELP_SHOP_MODULE_odExcludeCategories'             => 'Please separate id categories with comma - 1, 2, 67, 99',

    'SHOP_MODULE_odExcludeArticles'                  => 'List of the articles, where module will not be shown',
    'HELP_SHOP_MODULE_odExcludeArticles'             => 'Please separate id articles with comma - 1, 2, 67, 99',

    'SHOP_MODULE_GROUP_m'                           => 'Excluded categories',
    'SHOP_MODULE_GROUP_o'                           => 'Excluded categories',
    'SHOP_MODULE_GROUP_s'                           => 'Excluded categories',

    'SHOP_MODULE_m'                           => 'Excluded categories',
    'SHOP_MODULE_o'                           => 'Excluded categories',
    'SHOP_MODULE_s'                           => 'Excluded categories',
);
