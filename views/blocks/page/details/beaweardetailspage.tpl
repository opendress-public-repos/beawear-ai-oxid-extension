[{$smarty.block.parent}] 

[{oxscript include=$oViewConf->getModuleUrl('od/beawear','out/src/js/beawear-scan-integration.js')}] 

[{assign var=artCheck value="-"|explode:$oDetailsProduct->oxarticles__oxartnum->value}]

[{assign var=ExcludeCategories value=","|explode:$oConfig->getConfigParam('odExcludeCategories')}]

[{$oConfig->getConfigParam('odExcludeCategories')}]

[{assign var=ExcludeArticles value=","|explode:$oConfig->getConfigParam('odExcludeArticles')}]

[{$oConfig->getConfigParam('odExcludeArticles')}]

[{if trim($oDetailsProduct->oxarticles__oxartnum->value) != ''}]

<div id="beawear-target-container">
    <script defer>
        console.warn("Executed");
            fetch("https://dev-proxy-api.opendress.com/auth_token?shop=lovjoi", {
                body: `grant_type=password&username=None&password=None&client_id=None&client_secret=None`,
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/x-www-form-urlencoded",
                },
                method: "POST",
            })
                .then(async (response) => {
                    if (response.status === 200) {
                        const obj = await response.json();
                        const JWT = obj.access_token;
                        // create new user
                        const CREATE_USER_URL = "https://api-v2.opendress.com/v2/user";
                        const createUserResponse = await fetch(CREATE_USER_URL, {
                            body: `{}`,
                            headers: {
                                Accept: "application/json",
                                Authorization: "Bearer " + JWT,
                                "Content-Type": "application/json",
                            },
                            method: "POST",
                        });
                        if (createUserResponse.status === 201) {
                            let userItem = await createUserResponse.json();
                            return userItem["data"][0]
                        } else {
                            throw new Error(createUserResponse.statusText);
                        }
                    } else {
                        throw new Error(response.statusText);
                    }
                })
                .then((user) => {
                    const optionsObj = {
                        token: user["user_onetime_token"],
                        session_id: "session781",
                        shop_id: "",
                        matching_targets: ["size_table"],
                        articleIdentifier: { "shop_gtin_ean": "[{$oDetailsProduct->oxarticles__oxartnum->value}]" },
                    };
                    const resultsCallback = () => {};
                    BeawearScanIntegration.setup(optionsObj, resultsCallback, "https://app.opendress.com", true);
                    BeawearScanIntegration.addDefaultButton("beawear-target-container");
                });
    </script>
</div>

[{foreach from=$ExcludeCategories item=category}]
[{$category}]
[{if $actCategory->oxcategories__oxsort != '' && $category == $actCategory->oxcategories__oxsort}]
<script>document.getElementById("beawear-target-container").remove()</script>
[{php}]break;[{/php}]
[{/if}]
[{/foreach}]

[{foreach from=$ExcludeArticles item=article}]
[{$article}]
[{if $oDetailsProduct->oxarticles__oxartnum->value != '' && $article == $oDetailsProduct->oxarticles__oxartnum->value}]
<script>document.getElementById("beawear-target-container").remove()</script>
[{php}]break;[{/php}]
[{/if}]
[{/foreach}]

[{/if}]

