<?php

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
    'id'           => 'od',
    'title'        => 'Beawear',
    'description'  => array(
        'de' => 'Beawear',
        'en' => 'Beawear',
    ),
    'thumbnail'    => 'logo.webp',
    'version'      => '1.0.0',
    'author'       => 'OpenDress GmbH',
    'url'          => '',
    'email'        => '',
    'extend'       => array(),
    'controllers' => array(),
    'events'       => array(),
    'templates' => array('beaweardetailspage.tpl' => 'od/beawear/views/blocks/page/details/beaweardetailspage.tpl'),
    'blocks' => array(array('template' => 'page/details/inc/productmain.tpl',   'block' => 'details_productmain_tobasket', 'file' => '/views/blocks/page/details/beaweardetailspage.tpl')),
    'settings' => [
        [
            'group' => 'mainExclude',
            'name' => 'odExcludeCategories',
            'type' => 'str',
            'value' => '',
        ],
        [
            'group' => 'mainExclude',
            'name' => 'odExcludeArticles',
            'type' => 'str',
            'value' => '',
        ],
    ]
);
