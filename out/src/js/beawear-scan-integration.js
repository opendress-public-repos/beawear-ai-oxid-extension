var H=Object.defineProperty;var d=(k,D)=>H(k,"name",{value:D,configurable:!0});d(function(D,b){typeof exports=="object"&&typeof module=="object"?module.exports=b():typeof define=="function"&&define.amd?define([],b):typeof exports=="object"?exports.BeawearScanIntegration=b():D.BeawearScanIntegration=b()},"webpackUniversalModuleDefinition")(self,()=>(()=>{"use strict";var k={310:(h,u,g)=>{g.d(u,{Z:()=>x});var m=g(601),f=g.n(m),s=g(609),l=g.n(s),p=l()(f());p.push([h.id,`.Beawear-Scan-Button {\r
  background: transparent linear-gradient(90deg, #FBF2D5 0%, #F8EFEA 17%, #F1F1F9 36%, #E2F5FD 69%, #D0DCFB 100%) 0% 0% no-repeat padding-box;\r
  opacity: 1;\r
  border-radius: 5px;\r
  width: 100%;\r
  padding: 8px;\r
  display: flex;\r
  flex-direction: row;\r
  align-items: center;\r
  box-shadow: 5px 5px 5px #4a4a4a;\r
  padding: 0;\r
  overflow: hidden;\r
  border: 0;\r
  margin-top: 2rem;\r
  max-width:400px;\r
}\r
\r
.Beawear-Scan-Button:active {\r
  box-shadow: 2.5px 2.5px 5px #4a4a4a;\r
}\r
\r
.Beawear-Logo {\r
  height: 56px;\r
  width: 100%;\r
  object-fit: cover;\r
}\r
\r
.Beawear-Text {\r
  padding-left: 8px;\r
  text-align: left;\r
  color: #1D1D1B;\r
  font-family: Arial, Helvetica, sans-serif;\r
  font-size: 15px;\r
  line-height: 1.5;\r
}\r
\r
.Beawear-Bold-Text {\r
  font-weight: bold;\r
}\r
\r
.Beawear-Italic-Text {\r
  font-style: italic;\r
}\r
\r
.Beawear-App-Overlay {\r
  display: block !important;\r
  position: fixed;\r
  left: 0px;\r
  top: 0px;\r
  width: 100vw;\r
  height: 100vh;\r
  background: #000000;\r
  opacity: 0;\r
  z-index: -1;\r
  transition: 1s ease-in;\r
}\r
\r
.Beawear-App-Overlay-Active {\r
  opacity: 0.75;\r
  z-index: 1999;\r
  filter: blur();\r
}\r
\r
#Beawear-Scan-Frame {\r
  position: fixed;\r
  width: 35vw;\r
  height: 100vh;\r
  height: calc(var(--vh, 1vh) * 100);\r
  right: 0;\r
  top: 0;\r
  z-index: 2000;\r
  border:none;\r
}\r
\r
@media screen and (max-width: 820px) {\r
  #Beawear-Scan-Frame {\r
    width: 100%;\r
    right: 0;\r
    top: 0;\r
    margin-left: initial;\r
  }\r
}\r
`,""]);const x=p},609:h=>{h.exports=function(u){var g=[];return g.toString=d(function(){return this.map(function(f){var s="",l=typeof f[5]<"u";return f[4]&&(s+="@supports (".concat(f[4],") {")),f[2]&&(s+="@media ".concat(f[2]," {")),l&&(s+="@layer".concat(f[5].length>0?" ".concat(f[5]):""," {")),s+=u(f),l&&(s+="}"),f[2]&&(s+="}"),f[4]&&(s+="}"),s}).join("")},"toString"),g.i=d(function(f,s,l,p,x){typeof f=="string"&&(f=[[null,f,void 0]]);var A={};if(l)for(var S=0;S<this.length;S++){var B=this[S][0];B!=null&&(A[B]=!0)}for(var T=0;T<f.length;T++){var v=[].concat(f[T]);l&&A[v[0]]||(typeof x<"u"&&(typeof v[5]>"u"||(v[1]="@layer".concat(v[5].length>0?" ".concat(v[5]):""," {").concat(v[1],"}")),v[5]=x),s&&(v[2]&&(v[1]="@media ".concat(v[2]," {").concat(v[1],"}")),v[2]=s),p&&(v[4]?(v[1]="@supports (".concat(v[4],") {").concat(v[1],"}"),v[4]=p):v[4]="".concat(p)),g.push(v))}},"i"),g}},601:h=>{h.exports=function(u){return u[1]}},62:h=>{var u=[];function g(s){for(var l=-1,p=0;p<u.length;p++)if(u[p].identifier===s){l=p;break}return l}d(g,"getIndexByIdentifier");function m(s,l){for(var p={},x=[],A=0;A<s.length;A++){var S=s[A],B=l.base?S[0]+l.base:S[0],T=p[B]||0,v="".concat(B," ").concat(T);p[B]=T+1;var C=g(v),I={css:S[1],media:S[2],sourceMap:S[3],supports:S[4],layer:S[5]};if(C!==-1)u[C].references++,u[C].updater(I);else{var L=f(I,l);l.byIndex=A,u.splice(A,0,{identifier:v,updater:L,references:1})}x.push(v)}return x}d(m,"modulesToDom");function f(s,l){var p=l.domAPI(l);p.update(s);var x=d(function(S){if(S){if(S.css===s.css&&S.media===s.media&&S.sourceMap===s.sourceMap&&S.supports===s.supports&&S.layer===s.layer)return;p.update(s=S)}else p.remove()},"updater");return x}d(f,"addElementStyle"),h.exports=function(s,l){l=l||{},s=s||[];var p=m(s,l);return d(function(A){A=A||[];for(var S=0;S<p.length;S++){var B=p[S],T=g(B);u[T].references--}for(var v=m(A,l),C=0;C<p.length;C++){var I=p[C],L=g(I);u[L].references===0&&(u[L].updater(),u.splice(L,1))}p=v},"update")}},793:h=>{var u={};function g(f){if(typeof u[f]>"u"){var s=document.querySelector(f);if(window.HTMLIFrameElement&&s instanceof window.HTMLIFrameElement)try{s=s.contentDocument.head}catch{s=null}u[f]=s}return u[f]}d(g,"getTarget");function m(f,s){var l=g(f);if(!l)throw new Error("Couldn't find a style target. This probably means that the value for the 'insert' parameter is invalid.");l.appendChild(s)}d(m,"insertBySelector"),h.exports=m},173:h=>{function u(g){var m=document.createElement("style");return g.setAttributes(m,g.attributes),g.insert(m,g.options),m}d(u,"insertStyleElement"),h.exports=u},892:(h,u,g)=>{function m(f){var s=g.nc;s&&f.setAttribute("nonce",s)}d(m,"setAttributesWithoutAttributes"),h.exports=m},36:h=>{function u(f,s,l){var p="";l.supports&&(p+="@supports (".concat(l.supports,") {")),l.media&&(p+="@media ".concat(l.media," {"));var x=typeof l.layer<"u";x&&(p+="@layer".concat(l.layer.length>0?" ".concat(l.layer):""," {")),p+=l.css,x&&(p+="}"),l.media&&(p+="}"),l.supports&&(p+="}");var A=l.sourceMap;A&&typeof btoa<"u"&&(p+=`
/*# sourceMappingURL=data:application/json;base64,`.concat(btoa(unescape(encodeURIComponent(JSON.stringify(A))))," */")),s.styleTagTransform(p,f,s.options)}d(u,"apply");function g(f){if(f.parentNode===null)return!1;f.parentNode.removeChild(f)}d(g,"removeStyleElement");function m(f){var s=f.insertStyleElement(f);return{update:d(function(p){u(s,f,p)},"update"),remove:d(function(){g(s)},"remove")}}d(m,"domAPI"),h.exports=m},464:h=>{function u(g,m){if(m.styleSheet)m.styleSheet.cssText=g;else{for(;m.firstChild;)m.removeChild(m.firstChild);m.appendChild(document.createTextNode(g))}}d(u,"styleTagTransform"),h.exports=u}},D={};function b(h){var u=D[h];if(u!==void 0)return u.exports;var g=D[h]={id:h,exports:{}};return k[h](g,g.exports,b),g.exports}d(b,"__webpack_require__"),b.n=h=>{var u=h&&h.__esModule?()=>h.default:()=>h;return b.d(u,{a:u}),u},b.d=(h,u)=>{for(var g in u)b.o(u,g)&&!b.o(h,g)&&Object.defineProperty(h,g,{enumerable:!0,get:u[g]})},b.o=(h,u)=>Object.prototype.hasOwnProperty.call(h,u),b.nc=void 0;var M={};return(()=>{b.d(M,{default:()=>G});var h=b(62),u=b.n(h),g=b(36),m=b.n(g),f=b(793),s=b.n(f),l=b(892),p=b.n(l),x=b(173),A=b.n(x),S=b(464),B=b.n(S),T=b(310),v={};v.styleTagTransform=B(),v.setAttributes=p(),v.insert=s().bind(null,"head"),v.domAPI=m(),v.insertStyleElement=A();var C=u()(T.Z,v);const I=T.Z&&T.Z.locals?T.Z.locals:void 0;var L=function(n,a,r,o){function i(e){return e instanceof r?e:new r(function(c){c(e)})}return d(i,"adopt"),new(r||(r=Promise))(function(e,c){function y(_){try{t(o.next(_))}catch(E){c(E)}}d(y,"fulfilled");function w(_){try{t(o.throw(_))}catch(E){c(E)}}d(w,"rejected");function t(_){_.done?e(_.value):i(_.value).then(y,w)}d(t,"step"),t((o=o.apply(n,a||[])).next())})},F=function(n,a){var r={label:0,sent:function(){if(e[0]&1)throw e[1];return e[1]},trys:[],ops:[]},o,i,e,c;return c={next:y(0),throw:y(1),return:y(2)},typeof Symbol=="function"&&(c[Symbol.iterator]=function(){return this}),c;function y(t){return function(_){return w([t,_])}}function w(t){if(o)throw new TypeError("Generator is already executing.");for(;c&&(c=0,t[0]&&(r=0)),r;)try{if(o=1,i&&(e=t[0]&2?i.return:t[0]?i.throw||((e=i.return)&&e.call(i),0):i.next)&&!(e=e.call(i,t[1])).done)return e;switch(i=0,e&&(t=[t[0]&2,e.value]),t[0]){case 0:case 1:e=t;break;case 4:return r.label++,{value:t[1],done:!1};case 5:r.label++,i=t[1],t=[0];continue;case 7:t=r.ops.pop(),r.trys.pop();continue;default:if(e=r.trys,!(e=e.length>0&&e[e.length-1])&&(t[0]===6||t[0]===2)){r=0;continue}if(t[0]===3&&(!e||t[1]>e[0]&&t[1]<e[3])){r.label=t[1];break}if(t[0]===6&&r.label<e[1]){r.label=e[1],e=t;break}if(e&&r.label<e[2]){r.label=e[2],r.ops.push(t);break}e[2]&&r.ops.pop(),r.trys.pop();continue}t=a.call(n,r)}catch(_){t=[6,_],i=0}finally{o=e=0}if(t[0]&5)throw t[1];return{value:t[0]?t[1]:void 0,done:!0}}},P=function(){function n(){}return d(n,"ThokkThokkAdapter"),n.prototype.onSetup=function(a){return L(this,void 0,void 0,function(){var r,o,i,e,c,y,w;return F(this,function(t){for(console.log("searching for article identifiers..."),a.articleIdentifier=this.articleIdentifier,r=document.getElementsByTagName("script"),o=0;o<r.length;o++)if(i=r[o],i.type==="application/json"){e=i.innerText;try{if(c=JSON.parse(e),c.offers&&Array.isArray(c.offers)){if(y=c.offers.map(function(_){return _.sku}),y.length>0){w={shop_sku:y},a.articleIdentifier=w;break}}else console.log("No offers array found in JSON")}catch(_){console.log("Error parsing JSON:",_)}}return[2,a]})})},n.prototype.onResult=function(a,r){return L(this,void 0,void 0,function(){var o,i,e,c;return F(this,function(y){o=!0;try{for(i=document.getElementsByTagName("img"),e=0,c=i.length;e<c;++e)i[e].alt===a.data.payload.bestMatch.size_table_size_name&&(console.log(i[e].alt),i[e].click())}catch(w){o=!1,console.log("size selection failed with: "+w)}return r(a.data.payload),[2,o]})})},n}();const V=P;var z=function(n,a,r,o){function i(e){return e instanceof r?e:new r(function(c){c(e)})}return d(i,"adopt"),new(r||(r=Promise))(function(e,c){function y(_){try{t(o.next(_))}catch(E){c(E)}}d(y,"fulfilled");function w(_){try{t(o.throw(_))}catch(E){c(E)}}d(w,"rejected");function t(_){_.done?e(_.value):i(_.value).then(y,w)}d(t,"step"),t((o=o.apply(n,a||[])).next())})},U=function(n,a){var r={label:0,sent:function(){if(e[0]&1)throw e[1];return e[1]},trys:[],ops:[]},o,i,e,c;return c={next:y(0),throw:y(1),return:y(2)},typeof Symbol=="function"&&(c[Symbol.iterator]=function(){return this}),c;function y(t){return function(_){return w([t,_])}}function w(t){if(o)throw new TypeError("Generator is already executing.");for(;c&&(c=0,t[0]&&(r=0)),r;)try{if(o=1,i&&(e=t[0]&2?i.return:t[0]?i.throw||((e=i.return)&&e.call(i),0):i.next)&&!(e=e.call(i,t[1])).done)return e;switch(i=0,e&&(t=[t[0]&2,e.value]),t[0]){case 0:case 1:e=t;break;case 4:return r.label++,{value:t[1],done:!1};case 5:r.label++,i=t[1],t=[0];continue;case 7:t=r.ops.pop(),r.trys.pop();continue;default:if(e=r.trys,!(e=e.length>0&&e[e.length-1])&&(t[0]===6||t[0]===2)){r=0;continue}if(t[0]===3&&(!e||t[1]>e[0]&&t[1]<e[3])){r.label=t[1];break}if(t[0]===6&&r.label<e[1]){r.label=e[1],e=t;break}if(e&&r.label<e[2]){r.label=e[2],r.ops.push(t);break}e[2]&&r.ops.pop(),r.trys.pop();continue}t=a.call(n,r)}catch(_){t=[6,_],i=0}finally{o=e=0}if(t[0]&5)throw t[1];return{value:t[0]?t[1]:void 0,done:!0}}},W=function(){function n(){}return d(n,"ShopifyAdapter"),n.prototype.onSetup=function(a){return z(this,void 0,void 0,function(){var r,o,i,e,c,y;return U(this,function(w){switch(w.label){case 0:return w.trys.push([0,3,,4]),r=window.location.href.split("?")[0],o=r+".json",[4,fetch(o)];case 1:return i=w.sent(),[4,i.json()];case 2:return e=w.sent(),c=e.product.variants[0].barcode,a.articleIdentifier={shop_gtin_ean:c},[3,4];case 3:return y=w.sent(),console.log("couldn't parse data json"),[3,4];case 4:return[2,a]}})})},n.prototype.onResult=function(a,r){return z(this,void 0,void 0,function(){var o,i,e,c,y,w,t;return U(this,function(_){o=!0;try{for(i=a.data.payload.bestMatch.size_table_size_name.toLowerCase(),e=document.getElementsByClassName("variant-input"),c=0;c<e.length;c++)for(y=e[c],w=0,w=0;w<y.children.length;w++)try{if(i===y.children[w].textContent.toLowerCase()){t=y.getElementsByTagName("input")[0],t.click();break}}catch{console.log("not the right size")}}catch(E){o=!1,console.log("size selection failed with: "+E)}return r(a.data.payload),[2,o]})})},n}();const N=W;var O=function(n,a,r,o){function i(e){return e instanceof r?e:new r(function(c){c(e)})}return d(i,"adopt"),new(r||(r=Promise))(function(e,c){function y(_){try{t(o.next(_))}catch(E){c(E)}}d(y,"fulfilled");function w(_){try{t(o.throw(_))}catch(E){c(E)}}d(w,"rejected");function t(_){_.done?e(_.value):i(_.value).then(y,w)}d(t,"step"),t((o=o.apply(n,a||[])).next())})},R=function(n,a){var r={label:0,sent:function(){if(e[0]&1)throw e[1];return e[1]},trys:[],ops:[]},o,i,e,c;return c={next:y(0),throw:y(1),return:y(2)},typeof Symbol=="function"&&(c[Symbol.iterator]=function(){return this}),c;function y(t){return function(_){return w([t,_])}}function w(t){if(o)throw new TypeError("Generator is already executing.");for(;c&&(c=0,t[0]&&(r=0)),r;)try{if(o=1,i&&(e=t[0]&2?i.return:t[0]?i.throw||((e=i.return)&&e.call(i),0):i.next)&&!(e=e.call(i,t[1])).done)return e;switch(i=0,e&&(t=[t[0]&2,e.value]),t[0]){case 0:case 1:e=t;break;case 4:return r.label++,{value:t[1],done:!1};case 5:r.label++,i=t[1],t=[0];continue;case 7:t=r.ops.pop(),r.trys.pop();continue;default:if(e=r.trys,!(e=e.length>0&&e[e.length-1])&&(t[0]===6||t[0]===2)){r=0;continue}if(t[0]===3&&(!e||t[1]>e[0]&&t[1]<e[3])){r.label=t[1];break}if(t[0]===6&&r.label<e[1]){r.label=e[1],e=t;break}if(e&&r.label<e[2]){r.label=e[2],r.ops.push(t);break}e[2]&&r.ops.pop(),r.trys.pop();continue}t=a.call(n,r)}catch(_){t=[6,_],i=0}finally{o=e=0}if(t[0]&5)throw t[1];return{value:t[0]?t[1]:void 0,done:!0}}},J=function(){function n(){}return d(n,"BeawearScanIntegration"),n.setup=function(a,r,o,i){return O(this,void 0,void 0,function(){return R(this,function(e){switch(e.label){case 0:return[4,n._validateParams(a,r,i)];case 1:return e.sent(),i&&console.log("Running setup."),n._appUrl=new URL(o),n._optionsObj=a,n._resultCallback=r,n._enableDebugging=i,[4,n._addOverlay()];case 2:return e.sent(),this.addVhResizing(),a.shop_id!==10?[3,4]:(n._adapter=new V,[4,n._adapter.onSetup(a)]);case 3:return e.sent(),[3,8];case 4:return a.shop_id!==100?[3,6]:(n._enableDebugging&&console.log("creating shopify adapter for shopify demo shop"),n._adapter=new N,[4,n._adapter.onSetup(a)]);case 5:return e.sent(),[3,8];case 6:return a.shop_id!==102?[3,8]:(n._enableDebugging&&console.log("creating shopify adapter for LOVJOI"),n._adapter=new N,[4,n._adapter.onSetup(a)]);case 7:e.sent(),e.label=8;case 8:return[2]}})})},n.addDefaultButton=function(a){n.addButtonWithCssClasses(a,[n._CLASS_BUTTON])},n.addButtonWithCssClasses=function(a,r){n._enableDebugging&&console.log("Creating button.");var o=document.createElement("button");o.innerHTML=`
<img
class="`.concat(n._CLASS_IMAGE,`"
src="https://static.opendress.com/public/images/Logo.png"
alt="Beawear Logo"
/>
<div class="`).concat(n._CLASS_TEXT,` hello">
 <b class="`).concat(n._CLASS_BOLD,`">Jetzt mit BEAWEAR shoppen<br/>
 <i class="`).concat(n._CLASS_ITALIC,`">NEU:</i></b> Shoppe mit deinem digitalen Zwilling
</div>`),o.setAttribute('type','button'),o.classList.add(r),o.addEventListener("click",n._handleButtonClick),n._addCloseListener(),n._addDoneListener(),n._addGetDataListener();var i=n._getTargetById(a);i.append(o)},n.addVhResizing=function(){var a=window.innerHeight*.01;document.documentElement.style.setProperty("--vh","".concat(a,"px")),window.addEventListener("resize",function(){var r=window.innerHeight*.01;document.documentElement.style.setProperty("--vh","".concat(r,"px"))})},n._openScanFrame=function(){n._enableDebugging&&console.log("Opening scan frame."),n._toggleOverlay();var a=document.createElement("iframe");a.setAttribute("src",n._getUrl().toString()),a.id=n._ID_SCAN_FRAME,a.allow="camera",document.body.appendChild(a)},n._closeScanFrame=function(){n._enableDebugging&&console.log("Closing scan frame."),n._toggleOverlay();var a=n._getTargetById(n._ID_SCAN_FRAME);a!==null&&a.parentNode.removeChild(a)},n._addOverlay=function(){return O(this,void 0,void 0,function(){var a;return R(this,function(r){return a=document.createElement("div"),a.classList.add(n._CLASS_OVERLAY),document.body.append(a),[2]})})},n._toggleOverlay=function(){var a=n._getTargetByClass(n._CLASS_OVERLAY),r=n._CLASS_OVERLAY_ACTIVE;a&&(a.classList.contains(r)?a.classList.remove(r):a.classList.add(r))},n._getTargetById=function(a){n._enableDebugging&&console.log("Searching target: "+a);var r=document.getElementById(a);if(r)return r;throw new Error("Target container not found.")},n._getTargetByClass=function(a){n._enableDebugging&&console.log("Searching target: "+a);var r=document.querySelector("."+a);if(r)return r;throw new Error("Target container not found.")},n._sendMessageToScanFrame=function(a,r){var o=n._getTargetById(n._ID_SCAN_FRAME),i={message:a,payload:r};o.contentWindow.postMessage(i,"*")},n._validateMessage=function(a){n._enableDebugging&&console.log("Validating message from origin: "+a.origin);var r=n._getUrl().origin;if(a.origin!==r)return n._enableDebugging&&console.error("Received message is from unknown origin."),!1;var o=a.data;return typeof o!="object"?(console.error("Received message is not an object."),!1):o.message?!0:(console.error(`Received message has the wrong format.
        Please send an object formatted like this:
        {
        	 message: string,
        	 payload: object
        }`),!1)},n._handleButtonClick=function(){n._enableDebugging&&console.log("Button clicked."),n._openScanFrame()},n._addCloseListener=function(){window.addEventListener("message",function(a){n._validateMessage(a)&&a.data.message==="close"&&n._closeScanFrame()},!1)},n._addDoneListener=function(){window.addEventListener("message",function(a){n._validateMessage(a)&&a.data.message==="done"&&(n._closeScanFrame(),n._optionsObj.shop_id==10?(n._enableDebugging&&console.log("Here is: ",a),n._adapter.onResult(a,n._resultCallback)):n._optionsObj.shop_id===100?(n._enableDebugging&&console.log("size selection for shopify demo shop"),n._adapter.onResult(a,n._resultCallback)):n._optionsObj.shop_id===102&&(n._enableDebugging&&console.log("size selection for LOVJOI"),n._adapter.onResult(a,n._resultCallback)))},!1)},n._addGetDataListener=function(){window.addEventListener("message",function(a){n._validateMessage(a)&&a.data.message==="getData"&&(n._enableDebugging&&console.log("handle getData message"),n._sendMessageToScanFrame("options",n._getOptionsObj()))},!1)},n._getUrl=function(){if(n._appUrl)return n._appUrl;throw new Error("Scan integration not set up.")},n._getOptionsObj=function(){if(n._optionsObj)return n._optionsObj;throw new Error("Scan integration not set up.")},n._validateParams=function(a,r,o){return O(this,void 0,void 0,function(){return R(this,function(i){if(!(a&&a.token&&typeof a.token=="string"))throw new Error("Options Object invalid.");if(typeof r!="function")throw new Error("Result callback is not a function.");if(o==null)throw new Error("enableDebugging must not be null.");return n._enableDebugging&&console.log("Params okay."),[2]})})},n._CLASS_OVERLAY="Beawear-App-Overlay",n._CLASS_OVERLAY_ACTIVE="Beawear-App-Overlay-Active",n._CLASS_BUTTON="Beawear-Scan-Button",n._CLASS_IMAGE="Beawear-Logo",n._CLASS_TEXT="Beawear-Text",n._CLASS_BOLD="Beawear-Bold-Text",n._CLASS_ITALIC="Beawear-Italic-Text",n._ID_SCAN_FRAME="Beawear-Scan-Frame",n}();const G=J})(),M=M.default,M})());
